#ifndef PHYSICALPHENOMENON_H
#define PHYSICALPHENOMENON_H
#pragma once
#include "physicalphenomenon.h"


template<class T>
class PhysicalPhenomenon
{
public:
    PhysicalPhenomenon() {};
    virtual T operator()
        (int i, T* y, T t) = 0;
};

#endif // PHYSICALPHENOMENON_H
