TEMPLATE = app
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
        oscylatephenomenon.cpp \
        physicalphenomenon.cpp \
        rk_4.cpp

HEADERS += \
    oscylatephenomenon.h \
    physicalphenomenon.h \
    rk_4.h
