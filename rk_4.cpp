/*
#include "physicalphenomenon.h"
#include "rk_4.h"

template<class T>
T* RK_4<T>::rk_calculation(int N, PhysicalPhenomenon<T>* rk_metod, T* y, T t, T h, T* y_nast)
{

    T* y_tmp = new T[N];
    T* k1 = new T[N];
    T* k2 = new T[N];
    T* k3 = new T[N];
    T* k4 = new T[N];
    for (int i = 0; i < N; ++i)
    {
        k1[i] = h * (*rk_metod)(i, y, t);
        y_tmp[i] = y[i] + 0.5 * k1[i];
    }
    for (int i = 0; i < N; ++i)
    {
        k2[i] = h * (*rk_metod)(i, y_tmp, t + 0.5 * h);
        y_nast[i] = y[i] + 0.5 * k2[i];
    }
    for (int i = 0; i < N; ++i)
    {
        k3[i] = h * (*rk_metod)(i, y_nast, t + 0.5 * h);
        y_tmp[i] = y[i] + k3[i];
    }
    for (int i = 0; i < N; ++i)
    {
        k4[i] = h * (*rk_metod)(i, y_tmp, t + h);
        y_nast[i] = y[i] + (k1[i] + 2.0 * k2[i] + 2.0 * k3[i] + k4[i]) / 6.0;

    }
    delete[] k1;
    delete[] k2;
    delete[] k3;
    delete[] k4;

    delete[] y_tmp;

    return y_nast;
}
*/
