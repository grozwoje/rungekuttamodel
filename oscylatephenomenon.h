#ifndef OSCYLATEPHENOMENON_H
#define OSCYLATEPHENOMENON_H
#pragma once
#include "physicalphenomenon.h"

template<typename T>
class OscylatePhenomenon : public PhysicalPhenomenon<T>
{
public:
    OscylatePhenomenon() {};

    virtual T operator()(int i, T* y, T t) override
    {
        static const T w = 1;
        static const T b = 0;

        T wynik = 0;
        switch (i)
        {
        case 0:
            wynik = y[1];
            break;
        case 1:
            wynik = -w * w * y[0] - 2 * b * y[1];
            break;
        }
        return wynik;
    }

};
#endif // OSCYLATEPHENOMENON_H
