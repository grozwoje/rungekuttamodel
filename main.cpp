#include <iostream>
#include "physicalphenomenon.h"
#include "oscylatephenomenon.h"
#include "rk_4.h"


int main()
{


   PhysicalPhenomenon<float>* pOs = new OscylatePhenomenon<float>();
    int N = 2;

    float* y = new float[N];

    float* y_nast = new float[N];

    for (int i = 0; i < N; ++i)
    {
        y[i] = 3;
        y_nast[i] = 0;

    }
    double tmax = 5;
    double h = 0.1;

    for (double t = 0; t < tmax; t += h)
    {
        std::cout << "  ----OscylatePh : ----  " << std::endl;


        std::cout << " RK_4 : " << *RK_4<float>::rk_calculation(N, pOs, y, t, h, y_nast) << std::endl;

        std::cout << "  ---------------------  " << std::endl;
       // double* y_tmp = y;
        float* y_tmp = y;
        y = y_nast;
        y_nast = y_tmp;
    }

    delete[] y;
    delete[] y_nast;


    delete pOs;

    return 0;
}
